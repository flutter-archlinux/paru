# paru multi-arch OCI container images

Multi-arch OCI images of Arch Linux / Arch Linux on ARM with `base-devel`, `paru` and a non-superuser for builds.

This image is particularly useful when providing binary packages for Arch PKGBUILDs with complex dependencies for
multiple architectures.

### Supported architectures

- AArch64 / linux/arm64v8
- x86_64 / linux/amd64

The images are built on real hardware without instruction set emulation.

### Use of this image

```shell
docker run -ti --rm registry.gitlab.com/flutter-archlinux/paru:latest # or snapshot (e.g. 20240705)
[build@container /]# echo "${MFLAGS}" # the MFLAGS environment variable by default contains --ignorearch
[build@container /]# paru -B --mflags="${MFLAGS}" .
```

Available weekly snapshots : https://gitlab.com/flutter-archlinux/paru/container_registry/6563835

For sure, you can easily use this e.g. in GitLab CI.

### License

This piece of software is licensed under the terms and conditions of [EUPL-1.2](LICENSE).
