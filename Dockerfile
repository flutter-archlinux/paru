ARG BASE=menci/archlinuxarm:base-devel

FROM ${BASE}
LABEL authors="info@braid.business"

ENV MFLAGS="--ignorearch"

RUN pacman-key --init && pacman-key --populate

RUN useradd -m -s /usr/bin/false build

RUN sed -i 's/CheckSpace/#CheckSpace/g' /etc/pacman.conf
# ALARM always has issues with the package signatures
RUN bash -c "if [[ \"$(uname -m)\" == \"aarch64\" ]]; then sed -i 's/Required DatabaseOptional/Never/g' /etc/pacman.conf; fi"
RUN echo "build ALL=(ALL:ALL) NOPASSWD: ALL" >> /etc/sudoers.d/paru
RUN pacman -Syuy --noconfirm && pacman -S --noconfirm --needed sudo git && rm -rf /var/cache/pacman/pkg/*

RUN mkdir /tmp/paru
COPY PKGBUILD.paru.patch /
RUN git clone --depth 1 https://aur.archlinux.org/paru.git /tmp/paru && bash -c 'if [[ "$(uname -m)" == "aarch64" ]]; then patch -i /PKGBUILD.paru.patch -d /tmp/paru; fi' && pacman -Sy && cd /tmp/paru && chmod a+w . && sudo -u build makepkg -Asi --noconfirm && cd && rm -rf /tmp/paru /PKGBUILD.paru.patch /var/cache/pacman/pkg/*

USER build
